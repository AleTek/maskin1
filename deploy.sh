#!/bin/sh
COLOR=$(sh get_next_color.sh)
    consul-template -once -template "webapp-$COLOR.ctmpl:webapp.conf.tmp" && \
    cat webapp.conf.tmp > webapp.conf && \
    rm webapp.conf.tmp && \
    docker-compose kill -s HUP proxy && \
    curl --data $COLOR --request PUT http://localhost:8500/v1/kv/webapp/color
