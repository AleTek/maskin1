#!/bin/sh

COLOR=$(curl --silent http://localhost:8500/v1/kv/webapp/color?raw)

if [ "$COLOR" = "blue" ]; then
    echo "green"
else
    echo "blue"
fi
